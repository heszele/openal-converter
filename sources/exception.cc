#include <openal-converter/exception.hh>

namespace oal_converter
{
    char const* exception_type::what() const noexcept
    {
        return _message.c_str();
    }
}
