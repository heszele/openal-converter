#pragma once
#include <string>
#include <openal-converter/types.hh>
#include <openal-converter/exception.hh>

namespace oal_converter
{
    class decoder_type
    {
    public:
        class exception_type: public oal_converter::exception_type
        {
        public:
            using oal_converter::exception_type::exception_type;
        };

        decoder_type() = default;

        void open(std::string path);
        bool get_frame(const frame_pointer_type& frame);

    private:
        void create_format_context();
        void create_codec_context();
        void create_packet();

        void send_packet();
        bool receive_frame(const frame_pointer_type& frame);

        std::string _path;
        format_context_pointer_type _format_context;
        codec_context_pointer_type _codec_context;
        packet_pointer_type _packet;
        bool _flush = false;
    };
}
