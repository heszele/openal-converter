from conans import ConanFile, CMake, tools
import os
import subprocess
import re


DEVELOPMENT_VERSION = "dev"
CURRENT_VERSION = "v0.3"


def _get_ci_version():
    """
    Decided the wanted package verison in a Gitlab CI environment
    """
    if "CI_COMMIT_TAG" in os.environ.keys():
        return os.environ["CI_COMMIT_TAG"]
    elif "CI_COMMIT_BRANCH" in os.environ.keys():
        return os.environ["CI_COMMIT_BRANCH"]
    return DEVELOPMENT_VERSION


def _get_tag():
    """
    Gets the tag from git on the HEAD
    """
    process = subprocess.Popen(["git", "tag", "--contains", "HEAD"], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = process.communicate()
    if process.returncode != 0 or len(stdout) == 0:
        if len(stderr) != 0:
            print("Failed to get tag on HEAD! Error message: '{error}'".format(error=stderr.decode("utf-8")))
        return None
    tags = stdout.decode("utf-8").split("\n")
    # Even if there is only one tag on HEAD stdout is with a \n at the end
    # So tags will always have at least two elements,
    # but the second will be an empty string if there is only one tag in reality
    if len(tags) > 1 and len(tags[1]) != 0:
        print("More than one tags found on HEAD!")
        return None
    return tags[0]

def _get_branch():
    """
    Gets the name of the current branch from git
    """
    process = subprocess.Popen(["git", "status", "--porcelain", "--branch"], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = process.communicate()
    if process.returncode != 0 or len(stdout) == 0:
        if len(stderr) != 0:
            print("Failed to get branch name! Error message: '{error}'".format(error=stderr.decode("utf-8")))
        return None
    regexp = re.compile(r" (?P<local_branch>.*)\.\.\.(?P<remote>.*)/(?P<remote_branch>.*)\n")
    match = regexp.search(stdout.decode("utf-8"))
    if not match:
        print("Failed to parse git status: '{status}'!".format(status=stderr.decode("utf-8")))
        return None
    return match.group("local_branch")

def _get_development_version():
    """
    Returns the wanted package version in a development environment
    """
    tag = _get_tag()
    if tag is None:
        branch = _get_branch()
        if branch is None:
            return DEVELOPMENT_VERSION
        return branch
    return tag


def _get_package_version():
    """
    Returns the package version
    - In a CI environment we want to version the package based on tag or branch name
    - In a development environment we want to version the package based on the local repository's tag or branch name
    - Otherwise we return the current version of the package
    """
    if "CI_JOB_ID" in os.environ.keys():
        # We are running in a Gitlab CI
        return _get_ci_version()
    elif "OAL_CONVERTER_DEVELOPMENT" in os.environ.keys():
        # We are running in a development environment
        return _get_development_version()
    return CURRENT_VERSION


def _should_export_sources():
    return "CI_JOB_ID" in os.environ.keys() or "OAL_CONVERTER_DEVELOPMENT" in os.environ.keys()


class OpenAlConverterConan(ConanFile):
    name = "openal-converter"
    version = _get_package_version()
    license = "<Put the package license here>"
    author = "Rudolf Heszele rudolf.heszele@gmail.com"
    url = "https://gitlab.com/heszele/openal-converter.git"
    description = "Simple audio decoder and converter for OpenAL"
    topics = ("OpenAL", "audio", "decoder", "converter")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = ["cmake"]
    exports_sources = ["../*"] if _should_export_sources() else None
    _source_folder = "."
    build_requires = "ffmpeg/4.2.1@bincrafters/stable", "spdlog/1.8.2"
    requires = build_requires

    def source(self):
        if OpenAlConverterConan.exports_sources is None:
            self.run("git clone --branch {tag} https://gitlab.com/heszele/openal-converter.git {source_folder}".format(tag=OpenAlConverterConan.version,
                                                                                                                       source_folder=OpenAlConverterConan._source_folder))
        else:
            # We have everything exported by conan to the current folder, there is nothing to do
            pass

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        self.copy("*.hh", dst="include", src="install/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["openal_converter"]

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = "install"
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["OPENAL_CONVERTER_BUILD_TESTS"] = "OFF"
        cmake.configure(source_folder=OpenAlConverterConan._source_folder)

        return cmake
