#pragma once
#include <vector>
#include <string>
#include <fmt/format.h>
#include "memory.hh"
#include "types.hh"
#include "audio_buffer.hh"
#include "exception.hh"

namespace oal_converter
{
    class audio_file_type
    {
    public:
        class exception_type: public oal_converter::exception_type
        {
        public:
            using oal_converter::exception_type::exception_type;
        };

        static audio_buffer_type get_buffer(const std::string& path, channel_layout_type channel_layout, format_type format);

        explicit audio_file_type(const std::string& path);

        audio_buffer_type get_buffer(channel_layout_type channel_layout, format_type format);

    private:
        void store_frame(const frame_pointer_type& frame);

        frames_type _frames;
    };
}
