#include <openal-converter/details/resampler.hh>

extern "C"
{
    #include <libswresample/swresample.h>
}

namespace
{
    uint64_t convert_channel_layout(oal_converter::channel_layout_type channel_layout)
    {
        return channel_layout == oal_converter::channel_layout_type::mono ? AV_CH_LAYOUT_MONO : AV_CH_LAYOUT_STEREO;
    }

    AVSampleFormat convert_format(oal_converter::format_type format)
    {
        return format == oal_converter::format_type::u8 ? AV_SAMPLE_FMT_U8 : AV_SAMPLE_FMT_S16;
    }
}

namespace oal_converter
{
    resampler_type::resampler_type(channel_layout_type channel_layout, format_type format):
        _channel_layout(channel_layout),
        _format(format)
    {
        // Nothing to do yet
    }

    bool resampler_type::begin_resample(const frame_pointer_type& frame)
    {
        if(nullptr == frame)
        {
            throw exception_type("Invalid usage! You need to provide a valid frame!");
        }
        create_resample_context(frame);

        return nullptr != _resampler_context;
    }

    frame_pointer_type resampler_type::resample(const frame_pointer_type& frame) const
    {
        if(nullptr == frame)
        {
            throw exception_type("Invalid usage! You need to provide a valid frame!");
        }
        if(nullptr == _resampler_context)
        {
            // No need to resample anything
            return {};
        }

        const uint64_t channel_layout = convert_channel_layout(_channel_layout);
        const AVSampleFormat format = convert_format(_format);

        // Prepare resampled frame. It needs to have the same buffer size as the original frame
        // so we need to create and initialize it here
        frame_pointer_type resampled_frame(av_frame_alloc());

        resampled_frame->channel_layout = channel_layout;
        resampled_frame->sample_rate = frame->sample_rate;
        resampled_frame->format = format;
        resampled_frame->nb_samples = frame->nb_samples;
        av_frame_get_buffer(resampled_frame.get(), 0);

        if(0 != swr_convert_frame(_resampler_context.get(), resampled_frame.get(), frame.get()))
        {
            throw exception_type("Failed to resample audio!");
        }

        return resampled_frame;
    }

    void resampler_type::end_resample()
    {
        _resampler_context.reset();
    }

    void resampler_type::create_resample_context(const frame_pointer_type& frame)
    {
        const uint64_t channel_layout = convert_channel_layout(_channel_layout);
        const AVSampleFormat format = convert_format(_format);

        if(frame->channel_layout == channel_layout && frame->format == format)
        {
            // No need to resample
            return;
        }

        SwrContext* resampler_context = swr_alloc_set_opts(nullptr,                                      // we're allocating a new context
                                                           channel_layout,                               // out_ch_layout
                                                           format,                                       // out_sample_fmt
                                                           frame->sample_rate,                           // out_sample_rate
                                                           frame->channel_layout,                        // in_ch_layout
                                                           static_cast<AVSampleFormat>(frame->format),   // in_sample_fmt
                                                           frame->sample_rate,                           // in_sample_rate
                                                           0,                                            // log_offset
                                                           nullptr);                                     // log_ctx

        if(nullptr == resampler_context)
        {
            throw exception_type("Failed to allocate resample context!");
        }

        _resampler_context.reset(resampler_context);

        if(0 > swr_init(_resampler_context.get()))
        {
            throw exception_type("Failed to init resample context");
        }
    }
}
