#pragma once
#include <vector>
#include "types.hh"
#include "exception.hh"

namespace oal_converter
{
    class audio_buffer_type
    {
    public:
        class exception_type: public oal_converter::exception_type
        {
        public:
            using oal_converter::exception_type::exception_type;
        };

        using buffer_type = std::vector<uint8_t>;

        audio_buffer_type(channel_layout_type channel_layout,
                          format_type format,
                          int frequency = 44100,
                          buffer_type buffer = buffer_type());

        channel_layout_type get_channel_layout() const;

        format_type get_format() const;
        int get_frequency() const;
        const buffer_type& get_buffer() const;

        void set_frequency(int frequency);

        void add_frame(const frame_pointer_type& frame);

    private:
        bool is_valid(const frame_pointer_type& frame) const;

        channel_layout_type _channel_layout;
        format_type _format;
        int _frequency;
        buffer_type _buffer;
    };
}
