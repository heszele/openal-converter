#include <openal-converter/audio_buffer.hh>

extern "C"
{
    #include <libavformat/avformat.h>
}

namespace oal_converter
{
    audio_buffer_type::audio_buffer_type(channel_layout_type channel_layout,
                                         format_type format,
                                         int frequency,
                                         buffer_type buffer):
        _channel_layout(channel_layout),
        _format(format),
        _frequency(frequency),
        _buffer(std::move(buffer))
    {
        // Nothing to do yet
    }

    channel_layout_type audio_buffer_type::get_channel_layout() const
    {
        return _channel_layout;
    }

    format_type audio_buffer_type::get_format() const
    {
        return _format;
    }

    int audio_buffer_type::get_frequency() const
    {
        return _frequency;
    }

    const audio_buffer_type::buffer_type& audio_buffer_type::get_buffer() const
    {
        return _buffer;
    }

    void audio_buffer_type::set_frequency(int frequency)
    {
        _frequency = frequency;
    }

    void audio_buffer_type::add_frame(const frame_pointer_type& frame)
    {
        const AVSampleFormat format = static_cast<AVSampleFormat>(frame->format);
        const int size = frame->nb_samples * av_get_bytes_per_sample(format) * frame->channels;

        if(!is_valid(frame))
        {
            throw exception_type("Invalid frame to store in buffer!");
        }

        _buffer.insert(_buffer.end(), frame->extended_data[0], frame->extended_data[0] + size);
    }

    bool audio_buffer_type::is_valid(const frame_pointer_type& frame) const
    {
        if(frame->channel_layout != AV_CH_LAYOUT_STEREO && frame->channel_layout != AV_CH_LAYOUT_MONO)
        {
            return false;
        }
        if(frame->sample_rate != _frequency)
        {
            return false;
        }
        if(frame->format != AV_SAMPLE_FMT_S16 && frame->format != AV_SAMPLE_FMT_U8)
        {
            return false;
        }

        const channel_layout_type channel_layout = frame->channel_layout == AV_CH_LAYOUT_MONO ? channel_layout_type::mono : channel_layout_type::stereo;
        const format_type format = frame->format == AV_SAMPLE_FMT_U8 ? format_type::u8  : format_type::s16;

        return channel_layout == _channel_layout && format == _format;
    }
}
