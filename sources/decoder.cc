#include <openal-converter/details/decoder.hh>

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
}

namespace oal_converter
{
    void decoder_type::open(std::string path)
    {
        _path = std::move(path);
        create_format_context();
        create_codec_context();
    }

    bool decoder_type::get_frame(const frame_pointer_type& frame)
    {
        if(nullptr == frame)
        {
            throw exception_type("Invalid usage! You need to provide a valid frame!");
        }

        if(_flush)
        {
            return receive_frame(frame);
        }

        // Looks like we need to recreate the packet every time, otherwise av_read_frame will leak
        create_packet();

        const int result = av_read_frame(_format_context.get(), _packet.get());

        if(AVERROR_EOF == result)
        {
            // Try to flush the decoder
            _packet.reset();

            send_packet();

            _flush = true;

            return receive_frame(frame);
        }
        if(0 != result)
        {
            throw exception_type("Failed to read packet from {}", _path);
        }

        send_packet();

        return receive_frame(frame);
    }

    void decoder_type::create_format_context()
    {
        AVFormatContext* format_context = nullptr;

        if(0 != avformat_open_input(&format_context, _path.c_str(), nullptr, nullptr))
        {
            throw exception_type("Failed to open file at {}", _path);
        }
        if(nullptr == format_context)
        {
            throw exception_type("Failed to create format context for {}", _path);
        }

        _format_context.reset(format_context);

        if(0 > avformat_find_stream_info(format_context, nullptr))
        {
            throw exception_type("Failed to fetch stream info from {}", _path);
        }
    }

    void decoder_type::create_codec_context()
    {
        AVCodec* codec = nullptr;
        const int stream = av_find_best_stream(_format_context.get(), AVMEDIA_TYPE_AUDIO, -1, -1, &codec, 0);

        if(stream < 0)
        {
            throw exception_type("Failed to find audio stream in {}", _path);
        }

        AVCodecContext* codec_context = avcodec_alloc_context3(codec);

        if(nullptr == codec_context)
        {
            throw exception_type("Failed to allocate codec context");
        }

        _codec_context.reset(codec_context);

        if(0 > avcodec_parameters_to_context(_codec_context.get(), _format_context->streams[stream]->codecpar))
        {
            throw exception_type("Failed to copy codec parameters for {}", _path);
        }

        if(0 != avcodec_open2(_codec_context.get(), codec, nullptr))
        {
            throw exception_type("Failed to open codec for {}", _path);
        }
        if(_codec_context->channel_layout == 0)
        {
            if(_codec_context->channels > 2)
            {
                // FFMPEG was not able to figure out the channel layout. We can do that but only for mono and stereo files
                throw  exception_type("Internal error! Unable to figure out channel layout in {}", _path);;
            }
            _codec_context->channel_layout = _codec_context->channels == 1 ? AV_CH_LAYOUT_MONO : AV_CH_LAYOUT_STEREO;
        }
    }

    void decoder_type::create_packet()
    {
        _packet.reset(av_packet_alloc());

        if(nullptr == _packet)
        {
            throw exception_type("Failed to allocate packet for {}", _path);
        }
    }

    void decoder_type::send_packet()
    {
        if(0 != avcodec_send_packet(_codec_context.get(), _packet.get()))
        {
            throw exception_type("Failed to send packet to decoder for {}", _path);
        }
    }

    bool decoder_type::receive_frame(const frame_pointer_type& frame)
    {
        const int result = avcodec_receive_frame(_codec_context.get(), frame.get());

        if(0 != result && AVERROR_EOF != result)
        {
            throw exception_type("Failed to receive from from decoder for {}", _path);
        }

        return result == 0;
    }
}
