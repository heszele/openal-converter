#include <catch2/catch.hpp>
#include <openal-converter/details/resampler.hh>

extern "C"
{
    #include <libavutil/frame.h>
    #include <libavutil/channel_layout.h>
}

SCENARIO("Resampler will not do anything if not needed", "[resampler]")
{
    GIVEN("A frame with certain audio data")
    {
        const oal_converter::frame_pointer_type frame(av_frame_alloc());

        frame->sample_rate = 44100;
        frame->channel_layout = AV_CH_LAYOUT_MONO;
        frame->format = AV_SAMPLE_FMT_S16;

        AND_GIVEN("A resampler_type object with the same audio formar")
        {
            oal_converter::resampler_type resampler(oal_converter::channel_layout_type::mono,
                                                    oal_converter::format_type::s16);

            THEN("begin_resample() will return false, since the target format is the same")
            {
                CHECK_FALSE(resampler.begin_resample(frame));
                CHECK_FALSE(resampler.resample(frame));
            }
        }
        AND_GIVEN("A resampler_type object with a different audio formar")
        {
            oal_converter::resampler_type resampler(oal_converter::channel_layout_type::stereo,
                                                    oal_converter::format_type::s16);

            THEN("begin_resample() will return true, since the target format is different")
            {
                CHECK(resampler.begin_resample(frame));
                CHECK(resampler.resample(frame));
            }
        }
    }
}

SCENARIO("Resampler can resample frames", "[resampler]")
{
    GIVEN("A frame with certain audio data")
    {
        const oal_converter::frame_pointer_type frame(av_frame_alloc());

        frame->sample_rate = 44100;
        frame->channel_layout = AV_CH_LAYOUT_MONO;
        frame->format = AV_SAMPLE_FMT_S16;
        frame->nb_samples = 2000;
        av_frame_get_buffer(frame.get(), 0);

        AND_GIVEN("A resampler_type object with a different audio formar")
        {
            oal_converter::resampler_type resampler(oal_converter::channel_layout_type::stereo,
                                                    oal_converter::format_type::s16);

            THEN("begin_resample() will return true, since the target format is different")
            {
                CHECK(resampler.begin_resample(frame));

                oal_converter::frame_pointer_type resampled_frame = resampler.resample(frame);
                CHECK(resampled_frame);
                CHECK(resampled_frame->format == AV_SAMPLE_FMT_S16);
                CHECK(resampled_frame->sample_rate == 44100);
                CHECK(resampled_frame->channel_layout == AV_CH_LAYOUT_STEREO);
                CHECK(resampled_frame->nb_samples == 2000);
            }
        }
    }
}
