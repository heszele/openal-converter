#include <openal-converter/audio_file.hh>
#include <openal-converter/details/decoder.hh>
#include <openal-converter/details/resampler.hh>

extern "C"
{
    #include <libavcodec/avcodec.h>
}

namespace oal_converter
{
    audio_buffer_type audio_file_type::get_buffer(const std::string& path, channel_layout_type channel_layout, format_type format)
    {
        decoder_type decoder;
        resampler_type resampler(channel_layout, format);
        const frame_pointer_type frame(av_frame_alloc());
        audio_buffer_type buffer(channel_layout, format);

        decoder.open(path);

        if(!decoder.get_frame(frame))
        {
            return buffer;
        }
        buffer.set_frequency(frame->sample_rate);

        if(!resampler.begin_resample(frame))
        {
            // No need to resample
            buffer.add_frame(frame);
            while(decoder.get_frame(frame))
            {
                buffer.add_frame(frame);
            }

            return buffer;
        }

        // Resampling
        frame_pointer_type resampled_frame = resampler.resample(frame);
        buffer.add_frame(resampled_frame);
        
        while(decoder.get_frame(frame))
        {
            resampled_frame = resampler.resample(frame);
            
            buffer.add_frame(resampled_frame);
        }
        resampler.end_resample();

        return buffer;
    }

    audio_file_type::audio_file_type(const std::string& path)
    {
        decoder_type decoder;
        const frame_pointer_type frame(av_frame_alloc());

        decoder.open(path);
        while(decoder.get_frame(frame))
        {
            store_frame(frame);
        }
    }

    audio_buffer_type audio_file_type::get_buffer(channel_layout_type channel_layout, format_type format)
    {
        audio_buffer_type buffer(channel_layout, format);
        resampler_type resampler(channel_layout, format);

        if(_frames.empty())
        {
            return buffer;
        }
        buffer.set_frequency(_frames[0]->sample_rate);

        if(!resampler.begin_resample(_frames[0]))
        {
            // No need to resample! Just copy the decoded frames to buffer
            for(const frame_pointer_type& frame: _frames)
            {
                buffer.add_frame(frame);
            }

            return buffer;
        }

        for(const frame_pointer_type& frame: _frames)
        {
            frame_pointer_type resampled_frame = resampler.resample(frame);
            
            buffer.add_frame(resampled_frame);
        }
        resampler.end_resample();

        return buffer;
    }

    void audio_file_type::store_frame(const frame_pointer_type& frame)
    {
        AVFrame* clone = av_frame_clone(frame.get());

        if(nullptr == clone)
        {
            throw exception_type("Failed to clone frame!");
        }

        _frames.emplace_back(clone);
    }
}
