#include <catch2/catch.hpp>
#include <openal-converter/details/decoder.hh>
#include <openal-converter/exception.hh>

extern "C"
{
    #include <libavutil/frame.h>
}

SCENARIO("Decoders can open an existing file", "[decoder]")
{
    GIVEN("A decoder")
    {
        oal_converter::decoder_type decoder;

        THEN("It can open an existing wav file")
        {
            REQUIRE_NOTHROW(decoder.open("./sounds/test-mono.wav"));
            AND_THEN("It can decode frames")
            {
                const oal_converter::frame_pointer_type frame(av_frame_alloc());

                CHECK(decoder.get_frame(frame));
                CHECK(frame);
                CHECK(decoder.get_frame(frame));
                CHECK(frame);

                uint32_t frame_count = 2; // Since we have already read two frames
                while(decoder.get_frame(frame))
                {
                    CHECK(frame);
                    frame_count++;
                }
                CHECK(frame_count == 94);
            }
        }
        THEN("It can open an existing mp3 file")
        {
            REQUIRE_NOTHROW(decoder.open("./sounds/test-stereo.mp3"));
            AND_THEN("It can decode frames")
            {
                const oal_converter::frame_pointer_type frame(av_frame_alloc());

                CHECK(decoder.get_frame(frame));
                CHECK(frame);
                CHECK(decoder.get_frame(frame));
                CHECK(frame);

                uint32_t frame_count = 2; // Since we have already read two frames
                while(decoder.get_frame(frame))
                {
                    CHECK(frame);
                    frame_count++;
                }
                CHECK(frame_count == 256);
            }
        }
    }
}

SCENARIO("Decoders will throw an exception if the file cannot be opened", "[decoder]")
{
    GIVEN("A decoder")
    {
        oal_converter::decoder_type decoder;

        THEN("It will fail to open a non-existing file")
        {
            CHECK_THROWS_AS(decoder.open("./sounds/non-existing-file.mp3"), oal_converter::exception_type);
        }
    }
}
